﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class HandleInput : MonoBehaviour
{
    [SerializeField]
    private GameObject registerUsernameGameObject;

    [SerializeField]
    private GameObject registerPasswordGameObject;

    [SerializeField]
    private GameObject loginUsernameGameObject;

    [SerializeField]
    private GameObject loginPasswordGameObject;

    [SerializeField]
    private GameObject responseBodyTextGameObject;

    private InputField registerUsername;
    private InputField registerPassword;
    private InputField loginUsername;
    private InputField loginPassword;
    private Text responseBodyText;

    public void OnRegister()
    {
        if(registerPassword.text == "" || registerUsername.text == "")
        {
            Debug.Log("There is an empty data field in register zone!");
            responseBodyText.text = "ERROR: There are empty input fields";
        }

        else
        {
            StartCoroutine(Register(registerUsername.text, registerPassword.text));
        }
    }

    public void OnLogin()
    {
        if(loginPassword.text == "" || loginUsername.text == "")
        {
            Debug.Log("There is an empty data field in login zone!");
            responseBodyText.text = "ERROR: There are empty input fields";
        }

        else
        {
            StartCoroutine(Login(loginUsername.text, loginPassword.text));
        }
    }

    private void Awake()
    {
        registerUsername = registerUsernameGameObject.GetComponent<InputField>();
        registerPassword = registerPasswordGameObject.GetComponent<InputField>();
        loginPassword = loginPasswordGameObject.GetComponent<InputField>();
        loginUsername = loginUsernameGameObject.GetComponent<InputField>();
        responseBodyText = responseBodyTextGameObject.GetComponent<Text>();
    }

    private IEnumerator Register(string username, string password)
    {
        registerUsername.text = "";
        registerPassword.text = "";
        username = username.Trim();
        password = password.Trim();

        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost:8080/register", form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }

            else
            {
                Debug.Log(www.downloadHandler.text);
                responseBodyText.text = "Response body: " + www.downloadHandler.text;
            }
        }
    }

    private IEnumerator Login(string username, string password)
    {
        username = username.Trim();
        password = password.Trim();
        
        WWWForm form = new WWWForm();
        form.AddField("username", username);
        form.AddField("password", password);

        using (UnityWebRequest www = UnityWebRequest.Post("http://localhost:8080/login", form))
        {
            yield return www.SendWebRequest();
            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }

            else
            {
                // Debug.Log("Form upload complete!");
                Debug.Log(www.downloadHandler.text);
                responseBodyText.text = "Response body: " + www.downloadHandler.text;
                if(www.downloadHandler.text == "ok")
                {
                    loginUsername.text = "";
                    loginPassword.text = "";
                }
            }
        }
    }
}
